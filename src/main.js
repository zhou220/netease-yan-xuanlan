import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './styles/index.css' // 全局公共样式
import '@/vant/index' //vant按需引入

import * as API from '@/api/index'

Vue.prototype.$API = API
Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
