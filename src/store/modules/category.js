
import request from "../../utils/request"

const state = {
	getData:{},
	
}
const mutations = {
	SENTGETDATA(state,getData){
		state.getData =getData
	}
}
const actions = {
	// 轮播图数据
	async sendGetData({commit},val){
		const result = await request('wy/item/cateList.json?__timestamp=1630453776130&categoryId='+val)
		commit('SENTGETDATA',result)
	},

}
const getters = {
	
}

export default {
	state,
	mutations,
	actions,
	getters
}
