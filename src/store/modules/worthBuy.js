
// https://m.you.163.com/topic/v1/know/navWap.json

import request from "../../utils/request"

const state = {
	titleData:{},
	cardData:{},
}
const mutations = {
	GETTITLEDATA(state,titleData){
		state.titleData =titleData
	},
	GETCARDDATA(state,cardData){
		state.cardData =cardData
	}
}
const actions = {
	// 轮播图数据
	async getTitleData({commit}){
		const result = await request("wy/topic/v1/know/navWap.json")
		commit('GETTITLEDATA',result)
	},
	// 主页卡片数据
	async getCardData({commit},page){
		const result = await request("wy/topic/v1/find/recAuto.json?page="+page+"&size=2&exceptIds=")
		commit('GETCARDDATA',result)
	}

}
const getters = {
	
}

export default {
	state,
	mutations,
	actions,
	getters
}
