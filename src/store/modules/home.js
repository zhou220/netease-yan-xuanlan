const state = {
  homeInfo: {},
};
const mutations = {
  REVICE_HOMEINFO(state, homeInfo) {
    state.homeInfo = homeInfo;
  },
};
const actions = {
  async getHomeList({ commit }, timer) {
    const result = await this.$API.home.getHomeList(timer);
    if (result.code === "200") {
      commit("REVICE_HOMEINFO", result.data.data);
    }
  },
};
const getters = {
  //首页轮播
  focusList(state) {
    var homeInfo = state.homeInfo || {};
    return homeInfo.focusList || [];
  },
  //轮播下广告
  policyDescList(state) {
    var homeInfo = state.homeInfo || {};
    return homeInfo.policyDescList || [];
  },
  //网格区域
  kingKongModule(state) {
    var homeInfo = state.homeInfo || {};
    return homeInfo.kingKongModule || {};
  },
  //   kingKongList(state) {
  //     return state.homeInfo.kingKongModule.kingKongList || [];
  //   },
  //类目热销榜
  categoryHotSellModule(state) {
    var homeInfo = state.homeInfo || {};
    return homeInfo.categoryHotSellModule || {};
  },
  //outlets
  sceneLightShoppingGuideModule(state) {
    return state.homeInfo.sceneLightShoppingGuideModule || [];
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};
