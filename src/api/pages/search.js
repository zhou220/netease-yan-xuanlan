//搜索
import request from '../../utils/request'
export default {
	searchAutoComplete(value){
	return request({
		url:'/wy/xhr/search/searchAutoComplete.json',
		method:'post',
		data:{
			keywordPrefix:value
		},
		transformRequest: [function (data) {
			// Do whatever you want to transform the data
			let ret = ''
			for (let it in data) {
				// 如果要发送中文 编码
				ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
			}
			return ret
		}],
	
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
	})
	},
	getKeyword(){
		return request({
			url:'/wy/xhr/search/init.json',
			method:'get',
		})
	},
	getShopList(params){
		let timestamp=new Date().getTime()
		return request({
			url:`/wy/xhr/search/search.json?__timestamp=${timestamp}`,
			method:'get',
			params, // query参数
			data: {}, // 请求体参数
		})
	}
}
