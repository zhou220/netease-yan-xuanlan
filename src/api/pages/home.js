//首页
import request from "@/utils/request";
export default {
  // 首页推荐： https://m.you.163.com/xhr/index.json?__timestamp=1630131808529&
  getHomeList(timer) {
    return request.get(`/wy/xhr/index.json?__timestamp=${timer}&`);
  },
  getCategoryList() {
    return request.post(`/wy/xhr/page/global.json`);
  },
  getCardList(id) {
    return request.get(
      `/wy/item/list.json?__timestamp=1630487313551&categoryId=${id}`
    );
  },
};
