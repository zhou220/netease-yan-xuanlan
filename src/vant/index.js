import Vue from 'vue';
import {Button, Tabbar, TabbarItem,Search,Col,Row,Icon,Tag,List,Cell,CellGroup,DropdownMenu, DropdownItem,Grid, GridItem,
	Popup,Tab, Tabs,Sticky,Lazyload,Image as VanImage,Swipe, SwipeItem } from 'vant';

Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Button);
Vue.use(Search);
Vue.use(Col);
Vue.use(Row);
Vue.use(Icon);
Vue.use(Tag);
Vue.use(List);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(Popup);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Sticky);
Vue.use(Lazyload);
Vue.use(Icon);
Vue.use(VanImage);
Vue.use(Swipe);
Vue.use(SwipeItem);

